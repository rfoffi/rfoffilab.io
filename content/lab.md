---
title: Lab
---

# Media
- [Tryptone Broth (TrB)](/protocols/trb)
- [Terrific Broth (TB)](/protocols/tb)
- [Lysogeny Broth (LB)](/protocols/lb)
- [Marine Broth 2216 (MB2216)](/protocols/mb2216)
- [Artificial Seawater (ASW)](/protocols/asw)
- [Phosphate Buffer](/protocols/pb)
- [Phosphate Buffered Saline (PBS)](/protocols/pbs)
- [Tryptic Soy Broth (TSB)](/protocols/tsb)
- [Berg's Motility Buffer (BMB)](/protocols/bmb)


# Bacteria
- [Vibrio anguillarum 12B09 WT](/strains/12b09_wt)
- [Vibrio anguillarum 12B09 GFP](/strains/12b09_gfp)
- [Vibrio coralliilyticus YB2 WT](/strains/yb2_wt)
- [Vibrio coralliilyticus YB2 GFP](/strains/yb2_gfp)
- [Marinobacter adhaerens HP15 YFP](/strains/hp15_yfp)
- [Escherichia coli AB1157 WT](/strains/ab1157_wt)
- [Eschericha coli RP437 WT](/strains/rp437_wt)

# Other
- [Bovine Serum Albumin (BSA)](/protocols/bsa)
- [Cleaning microscope slides](/protocols/cleaning_microscope_slides)
