---
title: "Resources"
---

A collection of useful places on the web.

# Blogs
[Experimental History](https://experimentalhistory.substack.com/). \
[Statistical Biophysics Blog](http://statisticalbiophysicsblog.org/). \
[The Eighteenth Elephant](https://eighteenthelephant.com/). \
[Total Internal Reflection](https://totalinternalreflectionblog.com/). 

# Microscopy
[MicroscopyU](https://www.microscopyu.com). Microscopy education and online tools. \
[Photometrics - Learn](https://www.photometrics.com/learn). Educational resources, calculators and white papers from Photometrics.

# Bio
[Addgene](https://www.addgene.org/). Plasmid repository. \
[BioNumbers](https://bionumbers.hms.harvard.edu/search.aspx). Database of useful molecular biology numbers.

# Physics
[National Committee for Fluid Mechanics Films](https://web.mit.edu/hml/ncfmf.html). \
[SklogWiki](http://www.sklogwiki.org/SklogWiki/index.php/Main_Page). Statistical mechanics wiki. \
[Water Structure and Science](https://water.lsbu.ac.uk/water/). Everything about water.

# Seminars
[BPPB Virtual Seminars (2020)](https://iyerbiswas.com/outreach/bppbseminars/schedule/). Various topics in biophysics. \
[ICTP-SAIFR](https://www.youtube.com/@ictp-saifr7092). Plenty of recorded workshops, schools, minicourses.

# Tools
[ConnectedPapers](https://www.connectedpapers.com/). Explore connections between papers. \
[Dimensions](https://app.dimensions.ai/). Linked research database.


# Various
[AskNature](https://asknature.org/). Collection of strategies adopted by living beings. \
[Complexity Explorables](https://www.complexity-explorables.org/). Collection of interactive complex systems. \
[Our World in Data](https://ourworldindata.org/). Extensive datasets with nice visualizations and discussions.
