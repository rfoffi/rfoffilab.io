---
title: "How much should people trust scientists?"
date: 2024-02-21T18:33:38+01:00
draft: false
---

[A study conducted by the Pew Research Center in November 2023](https://www.pewresearch.org/science/2023/11/14/americans-trust-in-scientists-positive-views-of-science-continue-to-decline/) found that the trust of Americans in scientists has been continuously declining in the last 4 years.

Now, these results of course have many layers to unwrap, and that is not what I want to do here. Right after looking at this report, I asked myself another question, which I believe is more fundamental, and most likely has no correct answer.
Yes, that's the title of this post: *how much should people trust scientists?*

I think it's a tough one to answer, *especially* for a scientist.

Let's be more precise, following what the aforementioned study asked to its participants: *how much should people trust scientists to act in the best interests of the public?*.

It's not trivial. In an ideal world, I would really love to say that everyone should be able to fully trust scientists to act in the public's best interests. Because they know how to collect data, how to analyse it, how to interpret it, and they could therefore provide wise guidance to the people and to the ruling class.
But that's clearly not the case, and it's easy to see why: scientists are humans!

As humans themselves, individual scientists are not perfect.
Even assuming the best of intentions, their actions will always, to some degree, be affected by personal beliefs and biases and "returns" of some sort.
We have seen it [again](https://www.science.org/content/blog-post/luc-montagnier-not-losing-it-luc-montagnier-has-lost-it) and [again](https://skepticalinquirer.org/2007/09/aids-denialism-vs-science/): renowned scientists, who earned the trust of the wider public with great merits, can still spew copious amounts of bullshit and spread misinformation, and, if accidentally put in a position of power, they are definitely able to act against public interests, even in extremely dangerous ways.

If we take a scientist as an individual, an entity isolated from the broader scientific community, I don't see any special reason why they should be particularly worthy of trust.
But that is the point. A scientist is not an island. A scientist exists in the context of the scientific community who analyzes and judges their work. And the scientific community, which is the relatively abstract collective that ultimately establishes the "scientific consensus", better be worthy of trust!
Then a scientist, defined not as an individual, but as a representative of the scientific community within a well-defined area of scientific inquiry, will definitely deserve my trust.

But this is still tricky! Someone who is part of the scientific community may (sometimes) have, on the basis of their knowledge and involvement in the community, the ability to critically analyze another scientist's claims and actions. But that is a privilege that most (even other scientists well outside of their field of specialization) don't have. And then, what should one do? How can they contextualize the claims and actions of a scientist within those of the broader scientific community? How can they possibly judge something they don't have the tools nor the skills to judge?

This last question I think really is the crux of the problem. Any modern society that relies to some degree on science *requires* a high level of trust in scientists, primarily intended as a collective, but by extension as individuals, with the underlying assumption that the individuals represent the collective.
In this framework, whether that trust is well placed or not, decreasing levels of trust in scientists are problematic, because they undermine one of the pillars on which the society itself is founded.
If there is no public trust in science, at some point there may be no (good) science.
