---
title: Terrific Broth (TB)
---

# Recipe

| Substance                         | Concentration  | Notes | Inventory |
| ---                               | ---            | ---   | ---       |
| Tryptone                          | 20 g/L         |       | D88       |
| Yeast extract                     | 24 g/L         |       | D88       |
| Glycerol                          | 4 mL/L         |       | D88       |
| [Phosphate buffer](/protocols/pb) | 100 mL/L       |       |           |

For 1L:
- Dissolve 24 g of yeast extract, 20 g of tryptone and 4 mL of glycerol in 900 mL of deionized water.
- Autoclave at 120 C for 15 min
- Allow the solution to cool down and add 100 mL of sterile (0.2 μm filtered) phosphate buffer.
Can be stored at room temperature for at least 1 year.

TB is an extremely rich medium suitable for bacterial growth to high densities.
