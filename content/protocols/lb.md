---
title: Lysogeny Broth (LB)
---

# Recipe

| Substance        | Concentration   | Notes | Inventory |
| ---              | ---             | ---   | ---       |
| Tryptone         | 10 g/L          |       | D88       |
| Yeast extract    | 5 g/L           |       | D88       |
| NaCl             | 10 g/L          |       | D88       |

Simply add tryptone, yeast extract and NaCl in the provided concentration to deionized water.
Stir well and sterilize in autoclave at 120 C for 15 min.

Multiple formulations of LB exist, which differ in the amount of NaCl:
- LB (Miller): 10 g/L NaCl
- LB (Lennox): 5 g/L NaCl
- LB (Luria): 0.5 g/L NaCl

Tryptone provides essential amino acids; yeast extract provides a variety of organic compounds
including vitamins and trace elements; sodium chloride provides ions for transport and osmotic balance.
