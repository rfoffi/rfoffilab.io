---
title: Tryptone Broth (TrB)
---

# Recipe

| Substance        | Concentration  | Notes | Inventory |
| ---              | ---            | ---   | ---       |
| Tryptone         | 10 g/L         |       | D88       |
| NaCl             | 5 g/L          |       | D88       |

Simply add tryptone and NaCl in the provided concentration to deionized water.
Stir well and sterilize in autoclave at 120 C for 15 min.

Tryptone Broth is basically [Lysogeny Broth](/protocols/lb) without yeast extract.
It mainly provides essential amino acids to the bacteria, so it is typically used
as a medium to subculture bacteria that were pre-grown in LB.

The concentration of NaCl can be varied to match the different formulations of LB.
