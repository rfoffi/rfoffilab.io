---
title: Phosphate Buffer
---

# Recipe (1 M, pH 7)

| Substance | Concentration | Notes                          | Inventory                    |
| ---       | ---           | ---                            | ---                          |
| K2HPO4    | 1 M           | Molar mass = 174.18 g/mol      | 65 - chemical cupboard D92.1 |
| KH2PO4    | 185 mM        | Molar mass = 136.09 g/mol      | 64 - chemical cupboard D92.1 |

For 250 mL:
- fill a glass bottle with 250 mL deionized water
- dissolve 43.5 g of K2HPO4 in water
- dissolve 6.3 g of KH2PO4 in water
- resulting pH should be ~7, add more KH2PO4 to decrease pH or saturated NaOH to increase it
- autoclave 15 min at 120 C
