---
title: Berg's motility buffer (BMB)
---

# Recipe

| Substance                         | Concentration  | Notes | Inventory                    |
| ---                               | ---            | ---   | ---                          |
| EDTA                              | 0.1 mM         |       | 27 - chemical cupboard D92.1 |
| [Phosphate Buffer](/protocols/pb) | 10 mM          |       |                              |
| Lactic acid                       | 10 mM          |       |                              |
| L-methionine                      | 1 μM           |       |                              |

For 100 mL:
- prepare 100x stock solutions of the components (all of them should be autoclaved)
  - 1 M phosphate buffer
  - 1 M lactic acid (= 90 g/L)
  - 100 μM L-methionine (= 14.92 mg/L)
  - 10 mM EDTA (= 2.92 g/L)
- Add 1 mL of each component to 96 mL of deionized water
- Distribute the solution into 15 mL Falcon tubes

When washing cells in BMB before an experiment, add 0.02 wt% Tween 20 (= 2μl / 10 mL BMB)
or equivalent surfactant to avoid cells sticking to surfaces.

The buffer can be stored at room temperature.
Stocks can be stored long term in a 4C fridge.
