---
title: Quarto-reveal.js-Hugo
subtitle: An awesome threesome
author:
  name: Riccardo Foffi
  url: https://rfoffi.gitlab.io/
institute: ETH Zürich -- Institute for Environmental Engineering
format:
  revealjs:
    theme: dracula
    width: 1600
    height: 900
    transition: slide
    navigation-mode: linear
    slide-number: true
    auto-stretch: false
    auto-play-media: true
molstar: embed
---

```{julia}
import Pkg
Pkg.activate("makie"; shared=true)
using CairoMakie
function dracula(color::Symbol)
    if color == :background
        RGBf(40/255, 42/255, 54/255)
    elseif color == :foreground
        RGBf(284/255, 248/255, 242/255)
    elseif color == :red
        RGBf(255/255, 85/255, 85/255)
    elseif color == :orange
        RGBf(255/255, 184/255, 108/255)
    elseif color == :yellow
        RGBf(241/255, 250/255, 140/255)
    elseif color == :green
        RGBf(80/255, 250/255, 123/255)
    elseif color == :purple
        RGBf(189/255, 147/255, 249/255)
    elseif color == :cyan
        RGBf(139/255, 233/255, 253/255)
    elseif color == :pink
        RGBf(255/255, 121/255, 198/255)
    else
        RGBf(0, 0, 0)
    end
end
function theme_dracula()
    Theme(
        fontsize = 32,
        palette = ( color = dracula.([:purple,:orange,:cyan,:yellow,:pink,:green,:red]), ),
        backgroundcolor = dracula(:background),
        textcolor = dracula(:foreground),
        Axis = (
            xgridvisible = true,
            xgridstyle = :dash,
            xgridcolor = RGBAf(0,0,0,0.75),
            xgridwidth = 0.5,
            ygridvisible = true,
            ygridstyle = :dash,
            ygridcolor = RGBAf(0,0,0,0.75),
            ygridwidth = 0.5,
            xticksize = 0,
            yticksize = 0,
            xminorticksvisible = false,
            yminorticksvisible = false,
            xticksmirrored = true,
            yticksmirrored = true,
            titlefont = :bold,
            titlesize = 24,
            backgroundcolor = :transparent,
            spinewidth = 0.5,
            rightspinecolor = RGBAf(0,0,0,0.75),
            leftspinecolor = RGBAf(0,0,0,0.75),
            topspinecolor = RGBAf(0,0,0,0.75),
            bottomspinecolor = RGBAf(0,0,0,0.75),
        ),
        Legend = (
            framevisible = false,
            titlefont = :regular,
            titlesize = 28,
            labelsize = 28,
        ),
        Colorbar = (
            ticksvisible = false,
        ),
        Label = (
            font = :bold,
            fontsize = 32,
            halign = :center,
            padding = (0, 5, 5, 0),
        ),

        Lines = (
            linewidth = 2,
            cycle = Cycle([:color]),
        ),

        Scatter = (
            markersize = 16,
            alpha = 0.5,
            cycle = Cycle([:color]),
        ),

        Hist = (
            alpha = 0.5,
            cycle = Cycle([:color]),
        )

    )
end
set_theme!(theme_dracula())
```

# Julia computations
*Because I like it*

## My favorite function
:::: {layout-ncol=2}
```{julia}
#| label: fig-sinc
#| fig-cap: The sinc function.
x = range(-2π, +2π; length=5000)
lines(x, sinc.(x); linewidth=6, axis=(xlabel="x", ylabel="sinc(x)"))
```

:::{}
The sinc function is extremely cool and wiggly.

It appears in many areas of mathematics, physics and engineering.

I like it.
:::
::::

## Animation
::: {style="width:50%; height:50%;"}
```{julia}
fig = Figure()
ax = Axis(fig[1,1])
x = range(0, 2π; length=100)
t = Observable(0.0)
ω = 2π/10
y = @lift(@. sin(x - ω*$t))
lines!(ax, x, y, linewidth=5, color=dracula(:cyan))
anim = Record(fig, 1:100; framerate=10) do s
  t[] = s
end
display(anim)
```
:::
