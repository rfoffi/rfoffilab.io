---
title: "Resume"
---

## PhD
**ETH Zürich (IfU, Stockerlab) | September 2021 -- Current**\
I perform research on the motile and chemotactic behavior of marine
bacteria under the supervision of Prof. Roman Stocker,
Dr. Jonasz Słomka and Dr. Richard Henshaw.
I am developing numerical and theoretical frameworks to better understand
the effect of chemotaxis in regulating interactions between
bacteria and the phytoplankton (photosynthetic microorganisms
which account for at least half of the global oxygen production),
and new experimental methods to
observe bacterial motile behavior on scales inaccessible to
classical microscopy techniques.

## Junior Research Scholarship
**Sapienza Università di Roma | February 2021 -- August 2021**\
I performed numerical research on the structural properties
of supercooled water under the supervision of Prof. Francesco Sciortino.
Extending the results of my master's thesis, we developed a
technique to characterize different types of local molecular
environments through the topology of the hydrogen-bond network.
This scholarship led to the publication of two journal articles
(on *The Journal of Chemical Physics* and *Physical Review Letters*)
and allowed me to understand what it means to have
a great mentor. Thank you Francesco.

## Master's degree in Physics (Biosystems)
**Sapienza Università di Roma | December 2018 -- January 2021**\
Thesis: _Structural and topological properties of supercooled water_\
Thesis advisor: Prof. F. Sciortino

## Bachelor's degree in Physics
**Sapienza Università di Roma | September 2015 -- November 2018**\
Thesis: _Markov Chain Monte Carlo_\
Thesis advisor: Prof. E. Marinari
