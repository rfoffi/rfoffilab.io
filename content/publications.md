---
title: "Publications"
---

**Riccardo Foffi** and Francesco Sciortino.\
*Identification of local structures in liquid water from supercooled to ambient conditions*\
The Journal of Chemical Physics **160**, 094504 (2024) https://doi.org/10.1063/5.0188764\
\[[Full](https://rfoffi.gitlab.io/pubs/foffi_2024_identification_of_local_structures.pdf)\]
\[[SM](https://rfoffi.gitlab.io/pubs/foffi_2024_identification_of_local_structures_SM.pdf)\]
 
 

Isabella Daidone, **Riccardo Foffi**, Andrea Amadei, Laura Zanetti-Polzi.\
*A statistical mechanical model of supercooled water based on minimal clusters of correlated molecules*\
The Journal of Chemical Physics **159**, 094502 (2023) https://doi.org/10.1063/5.0157505\
\[[Full](https://rfoffi.gitlab.io/pubs/daidone_2023_a_statistical_mechanical_model_of.pdf)\]
\[[SM](https://rfoffi.gitlab.io/pubs/daidone_2023_a_statistical_mechanical_model_of_SM.pdf)\]

 
 

Jonasz Słomka, Uria Alcolombri, Francesco Carrara, **Riccardo Foffi**, François Peaudecerf, Matti Zbinden and Roman Stocker.\
*Encounter rates prime interactions between microorganisms*\
Interface Focus **13**, 20220059 (2023) https://doi.org/10.1098/rsfs.2022.0059\
\[[Full](https://rfoffi.gitlab.io/pubs/slomka_2023_encounter_rates_prime_interactions.pdf)\]

 
 

**Riccardo Foffi** and Francesco Sciortino.\
*Correlated Fluctuations of Structural Indicators Close to the Liquid-Liquid Transition in Supercooled Water*\
The Journal of Physical Chemistry B **127**, 378 (2022) https://doi.org/10.1021/acs.jpcb.2c07169\
\[[Full](https://rfoffi.gitlab.io/pubs/foffi_2022_correlated_fluctuations_of_structural.pdf)\]
\[[SM](https://rfoffi.gitlab.io/pubs/foffi_2022_correlated_fluctuations_of_structural_SM.pdf)\]

 
 

**Riccardo Foffi**, Francesco Sciortino, José Maria Tavares and Paulo Teixeira.\
*Building up DNA, bit by bit: a simple description of chain assembly*\
Soft Matter **17**, 10736 (2021) https://doi.org/10.10309/d1sm01130h\
\[[Full](https://rfoffi.gitlab.io/pubs/foffi_2021_building_up_dna_bit_by_bit.pdf)\]

 
 

**Riccardo Foffi** and Francesco Sciortino.\
*Structure of High-Pressure Supercooled and Glassy Water*\
Physical Review Letters **127**, 175502 (2021) https://doi.org/10.1103/PhysRevLett.127.175502\
\[[Full](https://rfoffi.gitlab.io/pubs/foffi_2021_structure_of_high-pressure_supercooled.pdf)\]
\[[SM](https://rfoffi.gitlab.io/pubs/foffi_2021_structure_of_high-pressure_supercooled_SM.pdf)\]

 
 

**Riccardo Foffi**, John Russo and Francesco Sciortino.\
*Structural and topological changes across the liquid-liquid transition in water*\
The Journal of Chemical Physics **154**, 184506 (2021) https://doi.org/10.1063/5.0049299\
\[[Full](https://rfoffi.gitlab.io/pubs/foffi_2021_structural_and_topological_changes.pdf)\]
\[[SM](https://rfoffi.gitlab.io/pubs/foffi_2021_structural_and_topological_changes_SM.pdf)\]
